# Welcome

## I'm Marcos Antônio Barbosa de Souza

:computer: I'm FullStack Developer!

:house_with_garden: I’m from Brazil.

:books: I’m currently learning everything.

:outbox_tray: 2018 Goals: create a new project and find a new job.

### ⚙️ GitHub Analytics

<table>
  <tr>
    <td>
      <img
        align="left"
        src="https://github-readme-stats.vercel.app/api?username=marcos-abs&theme=dark&hide_border=false&include_all_commits=true"
        alt="Github Stats"
      />
    </td>
    <td>
      <img
        align="left"
        src="https://github-readme-stats.vercel.app/api/top-langs/?username=marcos-abs&theme=dark&hide_border=false&include_all_commits=true&count_private=true&layout=compact"
        alt="Github Stats"
      />
    </td>
    <td>
      <br />
      <img
        align="left"
        src="https://github-readme-streak-stats.herokuapp.com/?user=marcos-abs&theme=dark&hide_border=false"
        alt="Github Stats"
      />
    </td>
  </tr>
</table>

--- 

### 🏆 GitHub Profile Trophy

<p align="center">
  <a
    href="https://github.com/ryo-ma/github-profile-trophy"
    title="repositório de troféus"
  >
    <img
      width="800"
      src="https://github-profile-trophy.vercel.app/?username=iuricode&column=8&theme=darkhub&no-frame=true&no-bg=true"
    />
  </a>
</p>

---

<div align="center">
  <h3><b>📍 Profile Visitor Count</b></h3>
</div>

<p align="center">
  <img
    src="https://profile-counter.glitch.me/iuricode/count.svg"
    alt="Número de visitantes no perfil"
  />
</p>

## About me

[![Github Badge](https://img.shields.io/badge/-Github-000?style=flat-square&logo=Github&logoColor=white&link=https://github.com/marcos-abs)](https://github.com/marcos-abs)
[![Gitlab Badge](https://img.shields.io/badge/-Gitlab-000?style=flat-square&logo=Gitlab&logoColor=white&link=https://gitlab.com/marcos-abs)](https://gitlab.com/marcos-abs)
[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-black?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/marcos-abs)](https://www.linkedin.com/in/marcos-abs)

- Thanks for visiting.

- Enjoy it!! \o/

## 𝗠𝘆 𝗧𝗲𝗰𝗸 𝗦𝘁𝗮𝗰𝗸

<table>
  <tbody>
    <tr valign="top">
      <td width="25%" align="center">
        <span>𝗝𝗮𝘃𝗮</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/java.svg">
      </td>
      <td width="25%" align="center">
        <span>𝗖#</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/c-sharp.svg">
      </td>
      <td width="25%" align="center">
        <span>PHP</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/php.svg">
      </td>
      <td width="25%" align="center">
        <span>Laravel</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/laravel.svg">
      </td>
    </tr>
    <tr valign="top">
      <td width="25%" align="center">
        <span>𝗛𝗧𝗠𝗟𝟱</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/html-5.svg">
      </td>
      <td width="25%" align="center">
        <span>𝗖𝗦𝗦𝟯</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/css-3.svg">
      </td>
      <td width="25%" align="center">
        <span>𝗝𝗮𝘃𝗮𝗦𝗰𝗿𝗶𝗽𝘁</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/javascript.svg">
      </td>
      <td width="25%" align="center">
        <span>JQuery</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/jquery.svg">
      </td>
    </tr>
    <tr valign="top">
      <td width="25%" align="center">
        <span>WordPress</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/wordpress.svg">
      </td>
      <td width="25%" align="center">
        <span>𝗩𝗦 𝗖𝗼𝗱𝗲</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/visual-studio-code.svg">
      </td>
      <td width="25%" align="center">
        <span>𝗚𝗶𝘁</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/git-icon.svg">
      </td>
      <td width="25%" align="center">
        <span>MySQL</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/mysql.svg">
      </td>
    </tr>
    <tr valign="top">
      <td width="25%" align="center">
        <span>SQLite</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/sqlite.svg">
      </td>
      <td width="25%" align="center">
        <span>MongoDB</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/mongodb.svg">
      </td>
      <td width="25%" align="center">
        <span>Apache</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/apache.svg">
      </td>
      <td width="25%" align="center">
        <span>Nginx</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/nginx.svg">
      </td>
    <tr valign="top">
      <td width="25%" align="center">
        <span>Debian</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/debian.svg">
      </td>
      <td width="25%" align="center">
        <span>RedHat</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/redhat.svg">
      </td>
      <td width="25%" align="center">
        <span>Ubuntu</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/ubuntu.svg">
      </td>
      <td width="25%" align="center">
        <span>CentOS</span><br><br><br>
        <img height="64px" src="https://cdn.svgporn.com/logos/centos.svg">
      </td>
    </tr>
    <tr valign="top">
      <td width="25%" align="center">
        <span>Octave GNU</span><br><br><br>
        <img height="64px" src="https://upload.wikimedia.org/wikipedia/commons/6/6a/Gnu-octave-logo.svg">
      </td>
      <td width="25%" align="center">
        <span>R Studio</span><br><br><br>
        <img height="64px" src="https://upload.wikimedia.org/wikipedia/commons/d/d0/RStudio_logo_flat.svg">
      </td>
      <td width="25%" align="center">
        <span>Python</span><br><br><br>
        <img height="64px" src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg">
      </td>
      <td width="25%" align="center">
        <span>Jupyter</span><br><br><br>
        <img height="64px" src="https://upload.wikimedia.org/wikipedia/commons/3/38/Jupyter_logo.svg">
      </td>
    </tr>
  </tbody>
</table>
